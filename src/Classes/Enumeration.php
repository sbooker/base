<?php

namespace Base\Classes;

use Base\Interfaces\Named;
use \Serializable;

/**
 * Parent of all enumeration classes.
 **/
abstract class Enumeration implements Named, Serializable
{
    const ANY_ID = 1;

    /**
     * @var array
     */
    protected $names = array(/* override me */);

    /**
     * @var int
     */
    private $id;

    final public function __construct($id = null)
    {
        $this->setId(is_null($id) ? static::getAnyId() : $id);
    }

    /**
     * @param $id
     * @return static
     */
    public static function create($id = null)
    {
        return new static($id);
    }

    public function serialize()
    {
        return (string) $this->id;
    }

    /**
     * @param string $serialized
     * @return $this
     */
    public function unserialize($serialized)
    {
        $this->setId($serialized);

        return $this;
    }

    /**
     * @return static[]
     */
    public static function getList()
    {
        return static::create()->getObjectList();
    }

    /**
     * must return any existent ID
     * 1 should be ok for most enumerations
     **/
    public static function getAnyId()
    {
        return self::ANY_ID;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return Enumeration
     * @throws \Exception
     */
    public function setId($id)
    {
        if ($this->itemExists($id)) {
            $this->id = $id;
        } else {
            throw new \InvalidArgumentException('knows nothing about such id == ' . $id);
        }

        return $this;
    }

    public function getName()
    {
        return $this->getNameList()[$this->id];
    }

    /**
     * @return static[]
     */
    public function getObjectList()
    {
        $list = array();
        $names = $this->getNameList();

        foreach (array_keys($names) as $id)
            $list[] = new $this($id);

        return $list;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string[]
     */
    public function getNameList()
    {
        return $this->names;
    }

    /**
     * @return array
     */
    public function getIdentifierList()
    {
        return array_keys($this->getNameList());
    }

    /**
     * @param $id
     * @return bool
     */
    public function itemExists($id)
    {
        return in_array($id, $this->getIdentifierList());
    }

    /**
     * @param mixed $id
     * @return bool
     */
    protected function is($id)
    {
        return $this->getId() == $id;
    }
}