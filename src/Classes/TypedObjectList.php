<?php

namespace Base\Classes;

use Base\Utils\Assert;

class TypedObjectList
{
    /** @var array */
    private $list = [];

    /**
     * @param [] $list
     */
    public function __construct(array $list, $className)
    {
        Assert::allItemsInstancesOf($list, $className);
        $this->list = $list;
    }

    /**
     * @return array
     */
    final public function getList()
    {
        return $this->list;
    }

    /**
     * @param mixed $id
     * @return object
     */
    final public function get($id)
    {
        foreach ($this->getList() as $item) {
            if ($item->getId() == $id) {
                return $item;
            }
        }

        Assert::isUnreachable();
    }
}