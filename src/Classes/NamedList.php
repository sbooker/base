<?php

namespace Base\Classes;

use Base\Interfaces\Named;

class NamedList extends TypedObjectList
{
    /**
     * @param Named[] $list
     */
    public function __construct(array $list)
    {
        parent::__construct($list, Named::class);
    }
}