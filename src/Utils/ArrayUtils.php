<?php

namespace Base\Utils;

use \Closure;
use \SimpleXMLElement;
use \InvalidArgumentException;
use \Base\Abstracts\StaticFactory;
use \Base\Interfaces\Identifiable;
use \Base\Interfaces\Named;
use \Base\Interfaces\Comparator;

final class ArrayUtils extends StaticFactory
{
    public static function get(array $arr, $key, $default = null)
    {
        return array_key_exists($key, $arr) ? $arr[$key] : $default;
    }

    /// orders $objects list by $ids order
    public static function regularizeList($ids, $objects)
    {
        if (!$objects)
            return array();

        $result = array();

        $objects = self::convertObjectList($objects);

        foreach ($ids as $id)
            if (isset($objects[$id]))
                $result[] = $objects[$id];

        return $result;
    }

    public static function convertObjectList($list = null)
    {
        $out = array();

        if (!$list)
            return $out;

        foreach ($list as $obj)
            /** @var Identifiable $obj */
            $out[$obj->getId()] = $obj;

        return $out;
    }

    public static function getIdsArray($objectsList)
    {
        $out = array();

        if (!$objectsList)
            return $out;

        Assert::isInstance(
            current($objectsList), Identifiable::class,
            'only identifiable lists accepted'
        );

        foreach ($objectsList as $object)
            /** @var Identifiable $object */
            $out[] = $object->getId();

        return $out;
    }

    /**
     * @param array $array
     * @return boolean
     */
    public static function isAssociative(array $array)
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }

    /**
     * @param array $objectsList
     * @param string $property
     * @return array
     */
    public static function getIdsAssocArray(array $objectsList, $property)
    {
        $out = array();
        if (empty($objectsList))
            return $out;

        foreach ($objectsList as $object) {
            Assert::isInstance(
                current($objectsList), Identifiable::class,
                'only identifiable lists accepted'
            );

            $getProperty = 'get' . ucfirst($property);
            /** @var Identifiable $object */
            $out[$object->getId()] = $object->$getProperty();
        }

        asort($out);

        return $out;
    }

    /**
     * @param array $objectsList
     * @param string $property
     * @return array
     * @throws InvalidArgumentException
     */
    public static function getAssocArrayByProperty(array $objectsList, $property = 'id')
    {
        $out = array();
        if (empty($objectsList))
            return $out;

        foreach ($objectsList as $object) {
            Assert::isInstance(
                current($objectsList), Identifiable::class,
                'only identifiable lists accepted'
            );

            $getProperty = 'get' . ucfirst($property);

            $out[$object->$getProperty()] = $object;
        }

        asort($out);

        return $out;
    }

    public static function mergeIdentifiableLists(/* ... */)
    {
        $arguments = func_get_args();

        Assert::isArray(reset($arguments));

        $result = array();
        foreach ($arguments as $list) {
            /** @var Identifiable $identifiable */
            foreach ($list as $identifiable) {
                Assert::isInstance($identifiable, Identifiable::class);
                $result[$identifiable->getId()] = $identifiable;
            }
        }

        return $result;
    }

    public static function &convertToPlainList($list, $key)
    {
        $out = array();

        foreach ($list as $obj)
            $out[] = $obj[$key];

        return $out;
    }

    public static function getArrayVar(&$array, $var)
    {
        if (isset($array[$var]) && !empty($array[$var])) {
            $out = &$array[$var];
            return $out;
        }

        return null;
    }

    public static function columnFromSet($column, array $array)
    {
        $result = array();

        foreach ($array as $row)
            if (isset($row[$column]))
                $result[] = $row[$column];

        return $result;
    }

    public static function mergeUnique(/* ... */)
    {
        $arguments = func_get_args();

        Assert::isArray(reset($arguments));

        return array_unique(
            call_user_func_array(
                'array_merge',
                $arguments
            )
        );
    }

    public static function countNonemptyValues($array)
    {
        Assert::isArray($array);
        $result = 0;

        foreach ($array as $value)
            if (!empty($value))
                ++$result;

        return $result;
    }

    public static function isEmpty(array $array)
    {
        foreach ($array as $key => $value)
            if ($value !== null)
                return false;

        return true;
    }

    /**
     * in: array(1, 2, 3, 4)
     * out: array(1 => array(2 => array(3 => 4)))
     **/
    public static function flatToDimensional($array)
    {
        if (!$array)
            return null;

        Assert::isArray($array);

        $first = array_shift($array);

        if (!$array)
            return $first;

        return array($first => self::flatToDimensional($array));
    }

    public static function mergeRecursiveUnique($one, $two)
    {
        if (!$one)
            return $two;

        Assert::isArray($one);
        Assert::isArray($two);

        $result = $one;

        foreach ($two as $key => $value) {

            if (is_integer($key)) {
                $result[] = $value;
            } elseif (
                isset($one[$key])
                && is_array($one[$key])
                && is_array($value)
            ) {
                $result[$key] = self::mergeRecursiveUnique($one[$key], $value);
            } else {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * @deprecated by array_combine($array, $array)
     **/
    public static function getMirrorValues($array)
    {
        Assert::isArray($array);

        $result = array();

        foreach ($array as $value) {
            Assert::isTrue(
                is_integer($value) || is_string($value),
                'only integer or string values accepted'
            );

            $result[$value] = $value;
        }

        return $result;
    }

    // TODO: drop Reflection
    public static function mergeSortedLists(
        $list1,
        $list2,
        Comparator $comparator,
        $compareValueGetter = null,
        $limit = null
    )
    {
        $list1Size = count($list1);
        $list2Size = count($list2);

        $i = $j = $k = 0;

        $newList = array();

        while ($i < $list1Size && $j < $list2Size) {
            if (
                $limit
                && $k == $limit
            )
                return $newList;

            if (!$compareValueGetter)
                $compareResult = $comparator->compare(
                    $list1[$i], $list2[$j]
                );
            else
                $compareResult = $comparator->compare(
                    $list1[$i]->{$compareValueGetter}(),
                    $list2[$j]->{$compareValueGetter}()
                );

            // list1 elt < list2 elt
            if ($compareResult < 0)
                $newList[$k++] = $list2[$j++];
            else
                $newList[$k++] = $list1[$i++];
        }

        while ($i < $list1Size) {
            if (
                $limit
                && $k == $limit
            )
                return $newList;

            $newList[$k++] = $list1[$i++];
        }

        while ($j < $list2Size) {
            if (
                $limit
                && $k == $limit
            )
                return $newList;

            $newList[$k++] = $list2[$j++];
        }

        return $newList;
    }

    /**
     * @param array $array
     * @param SimpleXMLElement $node
     */
    public static function toXml(array $array, SimpleXMLElement &$node)
    {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subNode = $node->addChild("$key");
                    self::toXml($value, $subNode);
                } else {
                    self::toXml($value, $node);
                }
            } else {
                $node->addChild("$key","$value");
            }
        }
    }

    public static function arrayRecursiveDiff($aArray1, $aArray2, $lookupField = null, $isIt = false)
    {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    if (is_null($aArray2[$mKey])) {
                        $aReturn[$mKey] = $mValue;
                    } elseif ($isIt && self::arrayRecursiveDiff($mValue, $aArray2[$mKey])) {
                        $aReturn[$mKey] = $mValue;
                    } else {
                        $aRecursiveDiff =
                            self::arrayRecursiveDiff(
                                $mValue,
                                $aArray2[$mKey],
                                $lookupField,
                                is_null($lookupField)
                                    ? false
                                    : $mKey == $lookupField
                            );

                        if (count($aRecursiveDiff)) {
                            $aReturn[$mKey] = $aRecursiveDiff;
                        }
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }

        return $aReturn;
    }

    /**
     * @param array $array - array of scalar items
     * @return bool
     */
    public static function isUnique(array $array)
    {
        $countValues = array_count_values($array);
        return count($countValues) == array_sum($countValues);
    }

    /**
     * @param Identifiable[] $array
     * @return bool
     */
    public static function isUniqueIdentifiable(array $array)
    {
        return self::isUnique(self::getIdsArray($array));
    }

    /**
     * @param array $array
     * @param Closure $callback in form of $key, $value => array($newKey, $newValue)
     * @return array
     */
    public static function arrayMapWithKeys(array $array, Closure $callback)
    {
        $return = array();

        foreach ($array as $key => $value) {
            list($newKey, $newValue) = $callback($key, $value);
            $return[$newKey] = $newValue;
        }

        return $return;
    }

    /**
     * @param Named[] $named
     * @return string[]
     */
    public static function getNamesArray(array $named)
    {
        Assert::allItemsInstancesOf($named, Named::class);

        return
            array_map(
                function (Named $object) {
                    return $object->getName();
                },
                $named
            );
    }

    /** @param array $array
     * @param callable|null $callback
     * @return array
     */
    public static function arrayFilterRecursive(array $array, $callback = null)
    {
        $newArray = array();

        foreach ($array as $key => $value) {
            $newArray[$key] = is_array($value) ? self::arrayFilterRecursive($value, $callback) : $value;
        }

        return
            $callback
                ? array_filter($newArray, $callback)
                : array_filter($newArray);
    }
}