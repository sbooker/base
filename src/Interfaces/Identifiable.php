<?php

namespace Base\Interfaces;

interface Identifiable
{
    /**
     * @return mixed
     */
    public function getId();
}