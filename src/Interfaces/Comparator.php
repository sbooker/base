<?php

namespace Base\Interfaces;

interface Comparator
{
    public function compare($one, $two);
}