<?php

namespace Base\Interfaces;

interface Named extends Identifiable
{
    /**
     * @return string
     */
    public function getName();
}