<?php

namespace Base\Abstracts;

use Base\Utils\Assert;

abstract class AbstractMap
{
    /** @var array */
    private $map = [];

    public function __construct(array $map = [])
    {
        foreach ($map as $key => $value) {
            $this->set($key, $value);
        }
    }

    /**
     * @param mixed $key
     * @param mixed $value
     * @return $this
     */
    final public function set($key, $value)
    {
        $this->assertKey($key);
        $this->assertValue($value);
        $this->map[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    final public function get($key)
    {
        $this->checkKey($key);
        return $this->map[$key];
    }

    /**
     * @param mixed $key
     * @throws \InvalidArgumentException
     * @return void
     */
    protected function assertKey($key)
    {
        Assert::isScalar($key);
    }

    /**
     * @param mixed $value
     * @throws \InvalidArgumentException
     * @return void
     */
    abstract protected function assertValue($value);

    /**
     * @param $key
     * @return void
     * @throws \InvalidArgumentException
     */
    final protected function checkKey($key)
    {
        Assert::isTrue(
            array_key_exists($key, $this->map),
            sprintf(
                'Key "%s" for mapping not found in "%s"',
                $key,
                get_called_class()
            )
        );
    }
}